package net.projecteuler;


public class SumOfMultiples {
	
	static int opers = 0;
	
	public static void main(String[] args) {
		System.out.println(sumOfMultiples(new int[]{3,5}, 1000));
		System.out.println("operations: " + opers);
	}

	public static int sumOfMultiples(int[] nums, int limit) {
		int sum = 0;
		
		for (int i = 1; i < limit; ++i, ++opers) {
			if ( (i%3 == 0) || (i%5 == 0) ) {
				sum += i;
			}
		}
		
		return sum;
	}

}
