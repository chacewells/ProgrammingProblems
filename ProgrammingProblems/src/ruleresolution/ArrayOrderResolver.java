package ruleresolution;

public class ArrayOrderResolver {
	
	public static void main(String[] args) {
        char[][] test1 = {{'x', 'z', 'y'}, {'y', 'f'}, {'z', 'y'}, {'x', 'z'}};
        char[][] test2 = {{'a', 'b', 'c'}, {'a', 'c'}};
        char[][] test3 = {{'a', 'b'}, {'b', 'c'}, {'a', 'c'}};
        
        String solution1 = new String(answer(test1));
        String solution2 = new String(answer(test2));
        String solution3 = new String(answer(test3));
        
        System.out.println("xzyf=" + solution1);
        System.out.println("abc=" + solution2);
        System.out.println("abc=" + solution3);
 }
 
	public static char[] answer(char[][] input) {
		char[] answer = new char[0];
	 // The answer here
		return answer;
	}

	
}
