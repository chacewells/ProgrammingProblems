package net.projecteuler.test;

import static org.junit.Assert.*;
import net.projecteuler.SumOfMultiples;

import org.junit.Test;

public class SumOfMultiplesTest {
	
	@Test
	public void sumOfMultiples3_5under10() {
		int[] nums = {3,5}; 
		int limit = 10, expected = 23;
		assertEquals(expected, SumOfMultiples.sumOfMultiples(nums, limit));
	}
	
	@Test
	public void sumOfMultiples3_5under20() {
		int[] nums = {3,5};
		int limit = 20, expected = 78;
		assertEquals(expected, SumOfMultiples.sumOfMultiples(nums, limit));
	}

}
